# SESCTO - Reserva de Hotel

Aplicativo desenvolvido para 3ª Etapa do Processo Seletivo 03/2018 SESCTO, referente ao cargo de Analista de Sistemas.

### Requisitos
O aplicativo foi desenvolvido usando a linguagem de programação java.
A seguintes ferramentas são necessárias para realizar a compilação do aplicativo:
  - JDK 1.8
  - Maven

### Installation e Execução

Compilando e executando a classe principal

```sh
$ mvn install
$ mvn exec:java -Dexec.mainClass=br.com.sescto.main.App
```
Comandos permitidos pelo aplicativo:
  - -i [caminho do arquivo csv]: importa um arquivo csv com dados dos hotéis
  -	-l [quantidade de registros] [pagina inicial] [pagina final]: exibe os hotéis importados de acordo com a páginação
 - -s [tipo de usuario] [datas separadas por virgula]: pesquisa o hotel mais barato de acordo com o tipo de usuário e as datas informadas
 - sair : termina a execução do programa