package br.com.sescto.bean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.sescto.dao.HotelListDao;
import br.com.sescto.modelo.ClienteCategoria;
import br.com.sescto.modelo.ClienteCategoriaValor;
import br.com.sescto.modelo.Hotel;
import br.com.sescto.repositorio.HotelRepositorio;
import br.com.sescto.util.HotelCSVReader;
import br.com.sescto.util.MensagemUtil;

/**
 * Classe responsável por coordenar as regras e validação dos comandos executados pelo usuário do sistema.  
 */
public class HotelController implements Serializable{
			
	private static final long serialVersionUID = 1L;
		
	private HotelListDao dao;
	private HotelRepositorio repositorio;
	
	private boolean comandoExit;
	
	public HotelController(){	
		this.dao = new HotelListDao();
		this.repositorio = new HotelRepositorio(dao);
	}	
	
	public HotelController(HotelListDao dao, HotelRepositorio repositorio) {		
		this.dao = dao;
		this.repositorio = repositorio;
	}

	/**
	 * Válida e processa o comando executado pelo usuário do sistema.
	 * Caso o comando não seja válido, argumentos fora das regras ou excções durante o processamento
	 * é mostrada uma mensagem amigável para o usuário.
	 * @param comando
	 * @param argumentos
	 * @throws Exception
	 */
	public String executarComando(String comando, String[] argumentos) throws Exception{		
		switch(comando){
			case "sair":
				comandoExit = true;
				return null;
			case "-s":
				validarArgumentos(argumentos, 3);
				return pesquisarHotel(argumentos[1], argumentos[2]);				
			case "-i":
				validarArgumentos(argumentos, 2);
				return importarHoteis(argumentos[1]);				
			case "-l":
				validarArgumentos(argumentos, 3);
				return listarHoteis(Integer.parseInt(argumentos[1]), Integer.parseInt(argumentos[2]));				
			default: throw new Exception(MensagemUtil.COMANDO_FALHO);
		}			
	}
	
	/**
	 * Lista os hotéis que foram importados ou adicionados pelo usuário.
	 * @param quantidade
	 * @param pagina
	 * @throws Exception
	 */
	public String listarHoteis(Integer quantidade, Integer pagina) throws Exception{
		try{
			List<Hotel> hoteis = repositorio.listarHoteis(quantidade, pagina);
			
			Locale locale = new Locale("pt", "BR");      
			NumberFormat realFormatter = NumberFormat.getCurrencyInstance(locale);
			
			String saidaListagem;
			
			saidaListagem = String.format("\n%-20s %-15s %-20s %-20s %-20s %-20s", "NOME DO HOTEL", "CLASSIFICAÇÃO", "VLR. SEMANA USR.", "VLR. FDS. USR.", "VLR. SEMANA COMER.", "VLR. FDS. COMER.") + "\n";
			for(Hotel h :  hoteis){
				ClienteCategoriaValor valorUsuario = h.getValores().get(ClienteCategoria.USUARIO);
				ClienteCategoriaValor valorComerciario = h.getValores().get(ClienteCategoria.COMERCIARIO);
				saidaListagem += String.format("%-20s %-15s %-20s %-20s %-20s %-20s", h.getNome(), h.getClassificacao(), realFormatter.format(valorUsuario.getValorSemana()),
						realFormatter.format(valorUsuario.getValorFinalSemana()), realFormatter.format(valorComerciario.getValorSemana()),
								realFormatter.format(valorComerciario.getValorFinalSemana())) + "\n";
			}
			return saidaListagem;
		}catch(Exception ex){
			throw ex;
		}
	}
	
	/**
	 * Importa dados de hotéis de um arquivo CSV enviando esses dados em uma lista de Modelos do tipo {@link Hotel},
	 * essa lista por sua vez é adicionado ao DAO para armazenar esses dados. 	
	 * @param caminho
	 * @throws Exception
	 */
	public String importarHoteis(String caminho) throws Exception{
		try{
			caminho = caminho.replace("\\", File.separator);					
			dao.inserir(HotelCSVReader.importarHoteisCsv(dao.contarQtdRegistros(), caminho.replaceAll("\"", "").replaceAll("'", "")));
			return MensagemUtil.HOTEL_IMPORTADO;
		}catch(FileNotFoundException ex){			
			throw new Exception(MensagemUtil.ARQUIVO_CSV_NAO_ENCONTRADO);
		}
		 
	}
	
	/**
	 * Pesquisa o hotel mais barato de acordo com as datas para hospedagem escolhidas e pela categoria do cliente.
	 * @param tipoUsuario
	 * @param datasEscolhidas
	 */
	public String pesquisarHotel(String tipoUsuario, String datasEscolhidas) throws Exception{		
		try {
			datasEscolhidas = datasEscolhidas.toLowerCase();
			String[] datasSplit = datasEscolhidas.split(",");
	
			List<Date> datas = new ArrayList<>();
	
			SimpleDateFormat formatador = new SimpleDateFormat("ddMMMyyyy");
	
			for (String dataString : datasSplit) {
				try {
					if(dataString.length() > 9){
						dataString = dataString.substring(0, 9);					
					}
					datas.add(formatador.parse(dataString));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}			
			ClienteCategoria clienteCategoria = HotelCSVReader.converteEnumCategoria(tipoUsuario.trim().replace(":", ""));
			Hotel hotel = this.repositorio.pesquisarHotelMenorPreco(clienteCategoria, datas);
			if(hotel != null){								
				return "Hotel mais barato: " + hotel.getNome();
			}else{
				throw new Exception(MensagemUtil.PESQUISA_HOTEL);
			}
			
		} catch (Exception e) {			
			throw e;
		}
				
	}
	
	/**
	 * Realiza a validação dos argumentos de um comando especifico
	 * @param argumentos
	 * @param quantidade
	 * @throws Exception
	 */
	public void validarArgumentos(String[] argumentos, int quantidade) throws Exception{
		if(argumentos.length < quantidade){
			throw new Exception(MensagemUtil.ARGUMENTOS_INVALIDOS);
		}
	}
	
	/**
	 * Mostra uma mensagem para o usuário do sistema
	 * @param msg
	 */
	public void addMensagem(String msg){
		System.out.println(msg);
	}
	
	public boolean isComandoExit() {
		return comandoExit;
	}

	public void setComandoExit(boolean comandoExit) {
		this.comandoExit = comandoExit;
	}	
}
