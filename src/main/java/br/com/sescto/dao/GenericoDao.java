package br.com.sescto.dao;

import java.util.List;

import br.com.sescto.modelo.Modelo;

public interface GenericoDao<T extends Modelo> {
	public void inserir(T entidade) throws GenericoDaoException;
	public void remover(T entidade) throws GenericoDaoException;
	public T pesquisarPorId(Long id);
	public List<T> pesquisarTodos();
	public List<T> pesquisarPaginado(Integer pagina, Integer qtdRegistrosPagina) throws GenericoDaoException;
	public Long contarQtdRegistros();
}
