package br.com.sescto.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.sescto.modelo.Hotel;
import br.com.sescto.util.MensagemUtil;


public class HotelListDao implements GenericoDao<Hotel>{
	
	private List<Hotel> hoteis;
		
	public HotelListDao() {
		this.hoteis = new ArrayList<>();
	}

	public HotelListDao(List<Hotel> hoteis) {	
		this.hoteis = hoteis;
	}

	@Override
	public void inserir(Hotel entidade) throws GenericoDaoException {
		if(!hoteis.contains(entidade)){
			hoteis.add(entidade);
		}else{
			throw new GenericoDaoException(MensagemUtil.HOTEL_EXISTENTE);
		}
	}
		
	public void inserir(List<Hotel> hoteis){		
		for(Hotel h : hoteis){
			try{
				inserir(h);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}			
	}

	@Override
	public void remover(Hotel entidade) throws GenericoDaoException {
		if(hoteis.contains(entidade)){
			hoteis.remove(entidade);
		}else{
			throw new GenericoDaoException(MensagemUtil.IMPOSSIVEL_REMOVER_HOTEL);
		}
	}

	@Override
	public Hotel pesquisarPorId(Long id) {
		if(hoteis != null){
			for(Hotel h : hoteis){
				if(h.getId().compareTo(id) == 0){
					return h;
				}
			}
		}		
		return null;
	}
	
	@Override
	public Long contarQtdRegistros() {
		if(hoteis == null){
			return 0L;
		}
		return (long) hoteis.size();
	}

	@Override
	public List<Hotel> pesquisarTodos() {
		return hoteis;
	}

	@Override
	public List<Hotel> pesquisarPaginado(Integer pagina, Integer qtdRegistrosPagina) throws GenericoDaoException {
		if(hoteis == null || hoteis.isEmpty()){
			throw new GenericoDaoException(MensagemUtil.HOTEIS_NAO_CADASTRADOS);
		}
		if(pagina < 1){
			pagina = 1;
		}				
		Integer valorInicial = (pagina - 1) * (qtdRegistrosPagina);		
		if(hoteis.size() <= valorInicial){
			throw new GenericoDaoException(MensagemUtil.HOTEIS_NAO_CADASTRADOS);
		}		
		Integer quantidadFinal = (valorInicial + qtdRegistrosPagina);
		if(quantidadFinal > hoteis.size()){
			quantidadFinal = hoteis.size();
		}
		return hoteis.subList(valorInicial, quantidadFinal);		
	}
	
}
