package br.com.sescto.dao;

public class GenericoDaoException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public GenericoDaoException(String message) {
		super(message);	
	}	
}
