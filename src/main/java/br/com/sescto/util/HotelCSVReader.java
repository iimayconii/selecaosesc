package br.com.sescto.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.sescto.modelo.ClienteCategoria;
import br.com.sescto.modelo.ClienteCategoriaValor;
import br.com.sescto.modelo.Hotel;

public class HotelCSVReader {

	/**
	 * Função responsavel por importas os dados de um csv dos Hoteis do SescTO
	 * 
	 * Os dados devem estar separados por ";"
	 * A informação do hotel em uma determinada linha que não seguir o tipo de dados especifico é ignorada.
	 * 
	 * Exemplo de uma linha de dados:
	 * 
	 * HOTEL 01;5;usuario;10;20;comerciario;5;10
	 * 
	 * @param caminhoCSV
	 * @return
	 */
	public static List<Hotel> importarHoteisCsv(Long idInicial, String caminhoCSV) throws FileNotFoundException, IOException{
		List<Hotel> hoteis = new ArrayList<Hotel>();	
		String line = "";
		String cvsSplitBy = ";";
		Long id = idInicial + 1;		
		try (BufferedReader br = new BufferedReader(new FileReader(caminhoCSV))) {

			while ((line = br.readLine()) != null) {
				String[] hoteisSplit = line.split(cvsSplitBy);

				try {
					if (hoteisSplit.length >= 8) {
						String nome = hoteisSplit[0].trim();
						if (nome == null || nome.isEmpty()) {
							throw new Exception();
						}
						Integer classificacao = Integer.parseInt(hoteisSplit[1]);

						ClienteCategoriaValor clienteUsuarioValor = new ClienteCategoriaValor(
								converteEnumCategoria(hoteisSplit[2].trim()), Double.parseDouble(hoteisSplit[3].trim()),
								Double.parseDouble(hoteisSplit[4].trim()));

						ClienteCategoriaValor clienteComerciarioValor = new ClienteCategoriaValor(
								converteEnumCategoria(hoteisSplit[5].trim()), Double.parseDouble(hoteisSplit[6].trim()),
								Double.parseDouble(hoteisSplit[7].trim()));

						hoteis.add(new Hotel(id++, nome, classificacao, clienteUsuarioValor, clienteComerciarioValor));

					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}catch(FileNotFoundException ex){
			throw ex;		
		} catch (IOException ex) {
			throw ex;		
		}
		return hoteis;
	}

	/**
	 * Converte um valor string para o tipo especifico de Enumeração
	 * @param valueOf
	 * @return
	 * @throws Exception
	 */
	public static ClienteCategoria converteEnumCategoria(String valueOf) throws Exception {
		ClienteCategoria categoriaUsuario = ClienteCategoria.valueOf(valueOf.toUpperCase());
		if (categoriaUsuario == null) {
			throw new Exception();
		}
		return categoriaUsuario;
	}
}
