package br.com.sescto.util;

public class MensagemUtil {
		
	public static final String AJUDA_COMANDO = "o sistema aceita os seguintes comandos:\n"
	+ "\n-i [caminho do arquivo csv]: importa um arquivo csv com dados dos hotéis;"
	+ "\n-l [quantidade de registros] [pagina inicial] [pagina final]: exibe os hotéis importados de acordo com a páginação;"
	+ "\n-s [tipo de usuario] [datas separadas por virgula]: pesquisa o hotel mais barato de acordo com o tipo de usuário e as datas informadas;"
	+ "\nsair : termina a execução do programa.\n";

	public static final String COMANDO_FALHO = "Olá usuário, infelizmente o comando informado não é suportado.\n" + AJUDA_COMANDO;	
	
	public static final String HOTEL_IMPORTADO = "Hoteis importados com sucesso!";
	public static final String ARQUIVO_CSV_NAO_ENCONTRADO = "O arquivo CSV não foi encontrado!";
	public static final String ARGUMENTOS_INVALIDOS = "Argumentos do comando está inválido!";
	public static final String HOTEL_EXISTENTE = "Hotel já existe na base de dados!";	
	public static final String HOTEIS_NAO_CADASTRADOS = "Não foram encontrados hotéis cadastrados!";
	public static final String IMPOSSIVEL_REMOVER_HOTEL = "Impossível remover um hotel que não está adicionado à base de dados!";
	public static final String PESQUISA_HOTEL = "Não foi possível realizar a pesquisa do hotel!";

}
