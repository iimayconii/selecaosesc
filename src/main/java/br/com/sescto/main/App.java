package br.com.sescto.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import br.com.sescto.bean.HotelController;
import br.com.sescto.util.MensagemUtil;

public class App {
	
	public static void main(String[] args) {
		
		System.out.print("Ajuda: " + MensagemUtil.AJUDA_COMANDO);
		
		HotelController hotelController = new HotelController(); 		
		while(!hotelController.isComandoExit()){			
			try{
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		        System.out.print("comando: ");
		        String comando = br.readLine();
		        String[] comandoSplit = comando.trim().split(" ");
	            String retorno = hotelController.executarComando(comandoSplit[0], comandoSplit);
	            if(retorno != null){
	            	System.out.println(retorno);
	            }
	        }catch(Exception ex){
	            System.out.println("Mensagem: " + ex.getMessage());
	        }
		}		

	}

}
