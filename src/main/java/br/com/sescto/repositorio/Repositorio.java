package br.com.sescto.repositorio;

import br.com.sescto.dao.GenericoDao;
import br.com.sescto.modelo.Modelo;

public abstract class Repositorio<T extends Modelo> {
	
	private GenericoDao<T> dao;
		
	public Repositorio(GenericoDao<T> dao) {		
		this.dao = dao;
	}

	protected GenericoDao<T> getDao() throws RepositorioException{
		if(dao == null){
			throw new RepositorioException("Acesso a lista de dados está nula");
		}		
		return dao;
	}

	protected void setDao(GenericoDao<T> dao) {
		this.dao = dao;
	}
	
}
