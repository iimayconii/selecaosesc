package br.com.sescto.repositorio;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import br.com.sescto.dao.GenericoDao;
import br.com.sescto.dao.GenericoDaoException;
import br.com.sescto.modelo.ClienteCategoria;
import br.com.sescto.modelo.Hotel;

/**
 * Classe contendo funções de pesquisa de hoteis na base de dados, definindo regras de negócios para 
 * realizar determinadas pesquisas.
 * 
 * @author Maycon A. J. Costa
 *
 */
public class HotelRepositorio extends Repositorio<Hotel> {

	public HotelRepositorio(GenericoDao<Hotel> dao) {
		super(dao);
	}

	/**
	 * Pesquisa o hotel com menor valor de acordo com a categoria do usuario e
	 * os dias de reserva
	 * 
	 * @param categoria
	 * @param datasDeReserva
	 * @return O Hotel mais barato
	 */
	public Hotel pesquisarHotelMenorPreco(ClienteCategoria categoria, List<Date> datasDeReserva)
			throws RepositorioException {
		List<Hotel> hoteis = (List<Hotel>) this.getDao().pesquisarTodos();
		if (hoteis.isEmpty()) {
			throw new RepositorioException("Não foram encontrados hoteis cadastrados");
		}
		Calendar cl = Calendar.getInstance();
		int qtdDiaSemanaTemp = 0;	
		for(Date data : datasDeReserva){
			cl.setTime(data);
			if(cl.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && cl.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
				qtdDiaSemanaTemp++;
			}
		}
		final int qtdDiaSemana = qtdDiaSemanaTemp;		
		final int qtdFinalSemana = datasDeReserva.size() - qtdDiaSemana;
		
		Hotel hotelMaisBarato = null;
		Double valorMaisBarato = Double.MAX_VALUE;
		for(Hotel hotel : hoteis){
			Double valorHotelAtual = (hotel.calculaValorCategoriaSemana(categoria, qtdDiaSemana))
					+ (hotel.calculaValorCategoriaFinalSemana(categoria, qtdFinalSemana));
			if(valorHotelAtual < valorMaisBarato){
				valorMaisBarato = valorHotelAtual;
				hotelMaisBarato = hotel;
			}else if(valorHotelAtual.compareTo(valorMaisBarato) == 0 && hotel.getClassificacao() > hotelMaisBarato.getClassificacao()){				
				if(hotel.getClassificacao() > hotelMaisBarato.getClassificacao()){
					hotelMaisBarato = hotel;
				}				
			}
			
		}		
		return hotelMaisBarato;
	}

	/**
	 * Lista os hotéis de acordo com as informações de páginação
	 * 
	 * @param pagina
	 * @param quantidade
	 * @return Lista de Hotéis
	 * @throws GenericoDaoException
	 * @throws RepositorioException
	 */
	public List<Hotel> listarHoteis(Integer pagina, Integer quantidade) throws GenericoDaoException, RepositorioException {
		return this.getDao().pesquisarPaginado(pagina, quantidade);		
	}
}
