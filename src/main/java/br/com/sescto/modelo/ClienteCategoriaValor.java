package br.com.sescto.modelo;

import java.io.Serializable;

/**
 * Classe que armazena os valores das diarias de um hotel de acordo com
 * seu dia da semana e categoria de cliente
 * 
 * @author Maycon A. J. Costa
 *
 */
public class ClienteCategoriaValor implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private ClienteCategoria categoria;
	private Double valorSemana;
	private Double valorFinalSemana;
	
	public ClienteCategoriaValor() {				
	}
	
	public ClienteCategoriaValor(ClienteCategoria categoria, Double valorSemana, Double valorFinalSemana) {		
		this.categoria = categoria;
		this.valorSemana = valorSemana;
		this.valorFinalSemana = valorFinalSemana;
	}

	public ClienteCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(ClienteCategoria categoria) {
		this.categoria = categoria;
	}

	public Double getValorSemana() {
		return valorSemana;
	}

	public void setValorSemana(Double valorSemana) {
		this.valorSemana = valorSemana;
	}

	public Double getValorFinalSemana() {
		return valorFinalSemana;
	}

	public void setValorFinalSemana(Double valorFinalSemana) {
		this.valorFinalSemana = valorFinalSemana;
	}

	@Override
	public String toString() {
		return "ClienteCategoriaValor [categoria=" + categoria + ", valorSemana=" + valorSemana + ", valorFinalSemana="
				+ valorFinalSemana + "]";
	}
	
	

}
