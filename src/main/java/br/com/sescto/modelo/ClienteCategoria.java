package br.com.sescto.modelo;

/**
 * Enumeração para definir a categoria de cliente
 * 
 * @author Maycon A. J. Costa
 *
 */
public enum ClienteCategoria {
	
	USUARIO("Usuário"), COMERCIARIO("Comerciário");	
	
	private ClienteCategoria(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
