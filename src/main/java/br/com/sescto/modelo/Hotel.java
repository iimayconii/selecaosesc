package br.com.sescto.modelo;

import java.util.HashMap;
import java.util.Map;

/**
 * Modelo que representa um hotel
 * 
 * @author Maycon Costa
 *
 */
public class Hotel extends Modelo {
		
	private static final long serialVersionUID = 1L;
		
	private String nome;
	private int classificacao;
	private Map<ClienteCategoria, ClienteCategoriaValor> valores = new HashMap<>();	
		
	public Hotel() {		
	}
	
	public Hotel(Long id, String nome, int classificacao, ClienteCategoriaValor ... valoresPorCategoriaCliente) {		
		this.setId(id);
		this.nome = nome;
		this.classificacao = classificacao;
		for(ClienteCategoriaValor valorCategoria : valoresPorCategoriaCliente){
			valores.put(valorCategoria.getCategoria(), valorCategoria);
		}		
	}

	public Hotel(String nome, int classificacao, ClienteCategoriaValor ... valoresPorCategoriaCliente) {			
		this.nome = nome;
		this.classificacao = classificacao;
		for(ClienteCategoriaValor valorCategoria : valoresPorCategoriaCliente){
			valores.put(valorCategoria.getCategoria(), valorCategoria);
		}		
	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(int classificacao) {
		this.classificacao = classificacao;
	}

	public Map<ClienteCategoria, ClienteCategoriaValor> getValores() {
		return valores;
	}

	public void setValores(Map<ClienteCategoria, ClienteCategoriaValor> valores) {
		this.valores = valores;
	}
	
	public Double calculaValorCategoriaSemana(ClienteCategoria clienteCategoria, final Integer qtdDias){
		if(this.valores.containsKey(clienteCategoria)){
			return valores.get(clienteCategoria).getValorSemana() * qtdDias;
		}
		return 0d;
	}
	
	public Double calculaValorCategoriaFinalSemana(ClienteCategoria clienteCategoria, final Integer qtdDias){
		if(this.valores.containsKey(clienteCategoria)){
			return valores.get(clienteCategoria).getValorFinalSemana() * qtdDias;
		}
		return 0d;
	}

	@Override
	public String toString() {
		return "Hotel [nome=" + nome + ", classificacao=" + classificacao + ", valores=" + valores + "]";
	}
		
}
