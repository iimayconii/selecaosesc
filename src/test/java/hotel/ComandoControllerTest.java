package hotel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.sescto.bean.HotelController;
import br.com.sescto.dao.HotelListDao;
import br.com.sescto.modelo.ClienteCategoria;
import br.com.sescto.modelo.ClienteCategoriaValor;
import br.com.sescto.modelo.Hotel;
import br.com.sescto.repositorio.HotelRepositorio;
import br.com.sescto.util.MensagemUtil;

public class ComandoControllerTest {
	
	HotelController hotelController;
	
	@Before
	public void popularBase(){		
		List<Hotel> hoteis = new ArrayList<>();
		hoteis.add(new Hotel(1l, "Hotel 01", 4, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 6d, 16d)));
		hoteis.add(new Hotel(2l, "Hotel 02", 5, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 7d, 18d)));
		
		hoteis.add(new Hotel(2l, "Hotel 03", 1, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 13d, 21d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 8d, 19d)));
	
		hoteis.add(new Hotel(2l, "Hotel 04", 2, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 14d, 23d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 9d, 10d)));
		
		hoteis.add(new Hotel(2l, "Hotel 05", 3, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 16d, 23d),	
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 10d, 13d)));
		
		HotelListDao hotelDao = new HotelListDao(hoteis);
		
		HotelRepositorio repositorio = new HotelRepositorio(hotelDao);
		
		hotelController = new HotelController(hotelDao, repositorio);			
		
	}

	@Test
	public void testarComandoPesquisarMenorPreco(){
		try {
			String retorno = hotelController.executarComando("-s", new String[]{"-s", "usuario:", "16Fev2018(seg),17Fev2018(terca),18Fev2018(quarta)"});
			Assert.assertEquals("Hotel mais barato: Hotel 02", retorno);
			
			retorno = hotelController.executarComando("-s", new String[]{"-s", "usuario:", "16Fev2018(seg),17Fev2018(terca),18Fev2018(quarta)"});
			Assert.assertNotEquals("Hotel mais barato: Hotel 01", retorno);
			
			retorno = hotelController.executarComando("-s", new String[]{"-s", "comerciario:", "16Fev2018(seg),17Fev2018(terca),18Fev2018(quarta)"});
			Assert.assertEquals("Hotel mais barato: Hotel 04", retorno);
			
			retorno = hotelController.executarComando("-s", new String[]{"-s", "comerciario:", "16Fev2018(seg),17Fev2018(terca),18Fev2018(quarta)"});
			Assert.assertNotEquals("Hotel mais barato: Hotel 02", retorno);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testarListagemDePesquisa(){
		try {
			String retorno = hotelController.executarComando("-l", new String[]{"-l", "0", "2"});			
			Assert.assertNotNull(retorno);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testarImportacao(){
		try {
			String retorno = hotelController.executarComando("-i", new String[]{"-i", this.getClass().getResource("/test_qtd_hoteis.csv").getFile()});			
			Assert.assertNotNull(retorno);
			Assert.assertEquals(MensagemUtil.HOTEL_IMPORTADO, retorno);
			
			retorno = hotelController.executarComando("-l", new String[]{"-l", "0", "100"});			
			Assert.assertNotNull(retorno);
			System.out.println(retorno);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
