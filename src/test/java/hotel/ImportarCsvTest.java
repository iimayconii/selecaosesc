package hotel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.sescto.modelo.Hotel;
import br.com.sescto.util.HotelCSVReader;

public class ImportarCsvTest {
	
	/**
	 * Realiza o teste de importação de dados de hoteis em um arquivo CSV.
	 * Nesse teste deve ser retornado a importação correta com 6 registros
	 */
	@Test
	public void testarImportacaoCsvQuantidade(){
		try {
			List<Hotel> hoteis = HotelCSVReader.importarHoteisCsv(0l, this.getClass().getResource("/test_qtd_hoteis.csv").getFile());
			Assert.assertNotNull(hoteis);
			Assert.assertEquals(6, hoteis.size());
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	/**
	 * Realiza o teste de importação de dados de hoteis em um arquivo CSV.
	 * Nesse teste deve ser retornado a importação correta com 3 registros, pois os outros 3 tem dados faltantes,
	 * desse modo o algoritmo de importação ignora essas linhas.
	 */
	@Test
	public void testarImportacaoCsvQuantidadeDadosFaltantes(){
		try {
			List<Hotel> hoteis = HotelCSVReader.importarHoteisCsv(0l, this.getClass().getResource("/test_qtd_hoteis_faltantes.csv").getFile());
			Assert.assertNotNull(hoteis);
			Assert.assertEquals(3, hoteis.size());
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
}
