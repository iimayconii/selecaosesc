package hotel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.sescto.dao.GenericoDao;
import br.com.sescto.dao.GenericoDaoException;
import br.com.sescto.dao.HotelListDao;
import br.com.sescto.modelo.ClienteCategoria;
import br.com.sescto.modelo.ClienteCategoriaValor;
import br.com.sescto.modelo.Hotel;

public class HotelDaoTest {
		
	private GenericoDao<Hotel> hotelDao;
	
	@Before
	public void popularBase(){		
		List<Hotel> hoteis = new ArrayList<>();
		hoteis.add(new Hotel(1l, "Hotel 01", 4, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 5d, 15d)));
		hoteis.add(new Hotel(2l, "Hotel 02", 4, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 5d, 15d)));
		hotelDao = new HotelListDao(hoteis);
	}
		
	@Test(expected = GenericoDaoException.class) 
	public void inserirErrorTest() throws GenericoDaoException{
		hotelDao.inserir(new Hotel(1l, "Hotel 01", 4, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 5d, 15d)));
	}	
	
	@Test
	public void pesquisarTest() throws GenericoDaoException{
		List<Hotel> pesquisa = hotelDao.pesquisarPaginado(1, 1);
		Assert.assertEquals(1, pesquisa.size());
	}
		
	@Test
	public void removerTest() throws GenericoDaoException{
		hotelDao.remover(new Hotel(2l, "Hotel 02", 4, 
				new ClienteCategoriaValor(ClienteCategoria.USUARIO, 10d, 20d),
				new ClienteCategoriaValor(ClienteCategoria.COMERCIARIO, 5d, 15d)));
		List<Hotel> hoteis = hotelDao.pesquisarTodos();
		Assert.assertEquals(1, hoteis.size());
		Hotel hotel = hoteis.get(0);
		Assert.assertEquals(hotel.getNome(), "Hotel 01");
	}			
}
